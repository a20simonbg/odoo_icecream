# -*- coding: utf-8 -*-

from odoo import models, fields, api


class odoo_icecream(models.Model):
    _name = 'odoo_icecream.odoo_icecream'
    _description = 'odoo_icecream.odoo_icecream'

    type = fields.Selection([
        {'Tarrina 1 bola', 'static/description/Tarrina1.png'},
        {'Tarrina 2 bolas','static/description/Tarrina2.png'},
        {'Tarrina 3 bolas','static/description/Tarrina3.png'},
        {'Cucurucho 1 bola','static/description/Cono1.png'},
        {'Cucurucho 2 bolas','static/description/Cono2.png'},
    ], required=True, default='Cucurucho 1 bola')
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
