from odoo import api, fields, models

class IceCream(models.Model):
    _name = "store.icecream"
    _description = "IceCream Store"
    
    type = fields.Selection([
        {'Tarrina 1 bola', 'static/description/Tarrina1.png'},
        {'Tarrina 2 bolas','static/description/Tarrina2.png'},
        {'Tarrina 3 bolas','static/description/Tarrina3.png'},
        {'Cucurucho 1 bola','static/description/Cono1.png'},
        {'Cucurucho 2 bolas','static/description/Cono2.png'},
    ], required=True, default='Cucurucho 1 bola')