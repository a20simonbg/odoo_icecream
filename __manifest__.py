# -*- coding: utf-8 -*-
{
    'name': "odoo_icecream",
    'summary': """IceCream Store Billing Manager""",
    'description': """A billing management softare for your icecream store""",
    'author': "Simón Barreiro Gómez",
    'website': "https://gitlab.com/a20simonbg",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Productivity',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base'],
    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
