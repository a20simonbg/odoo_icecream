# -*- coding: utf-8 -*-
{
    'name' : 'IceCream Billing Manager',
    'version' : '1.1',
    'summary' : 'IceCream Store Billing Manager',
    'sequence' : -100,
    'description' : """A billing management softare for your icecream store""",
    'category' : 'Productivity',
    'website' : 'https://github.com/manusimon',
    'license' : 'LGPL-3',
    'depends' : [],
    'data' : [],
    'demo' : [],
    'qweb' : [],
    'installable' : True,
    'application' : True,
    'auto-install' : False,
}